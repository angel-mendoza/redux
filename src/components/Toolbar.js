import React from 'react';
import { Link } from 'react-router-dom';


const Toolbar = (props)=>{
	return(
		<nav className="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
			<button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo01" aria-controls="navbarTogglerDemo01" aria-expanded="false" aria-label="Toggle navigation">
				<span className="navbar-toggler-icon"></span>
			</button>
			<div className="collapse navbar-collapse" id="navbarTogglerDemo01">
				<Link className="navbar-brand" to="/">Home</Link>
				<ul className="navbar-nav mr-auto mt-2 mt-lg-0">
					<li className="nav-item active">
						<Link className="nav-link" to="/tasks">Tasks <span className="sr-only">(current)</span></Link>
					</li>
				</ul>
				<form className="form-inline my-2 my-lg-0">
					<input className="form-control mr-sm-2" type="search" placeholder="Search" aria-label="Search"></input>
					<button className="btn btn-outline-primary my-2 my-sm-0" type="submit">Search</button>
				</form>
			</div>
		</nav>
	)
}

export default Toolbar