import React from 'react'

const ErrorMsg = (props) => {
	return (
		<div className="pages-error">
			<h5 className="error-msg-title">{props.error}</h5>
		</div>
	)
}

export default ErrorMsg;