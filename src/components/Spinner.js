import React, { Component } from 'react';

import '../styles/Spinner.css';

export default class Spinner extends Component {
  render() {
    return (
    	<div className="loader">Loading...</div>
    );
  }
}