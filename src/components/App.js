import React from 'react';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Users from '../pages/Users'
import Tasks from '../pages/Tasks'
import NewTask from '../pages/NewTask'
import Posts from '../pages/Posts'
import Layout from './Layout'
import NotFound from '../pages/NotFound'

const App = ()=>{
	return (
		<BrowserRouter>
			<Layout>
				<Switch>
					<Route exact path="/" component={Users} />
					<Route exact path="/tasks" component={Tasks} />
					<Route exact path="/tasks/new" component={NewTask} />
					<Route exact path="/tasks/edit/:user_id/:task_id" component={NewTask} />
					<Route exact path="/posts/:id" component={Posts} />
					<Route component={NotFound} />
				</Switch>
			</Layout>
		</BrowserRouter>
	);
}

export default App
