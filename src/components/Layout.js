import React from 'react';
import Toolbar from './Toolbar';

function Layout(props) {
  // const children = props.children;

  return (
    <React.Fragment>
      <Toolbar />
      {props.children}
    </React.Fragment>
  );
}

export default Layout;