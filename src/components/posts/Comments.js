import React from 'react'
import { connect } from 'react-redux'
import {Card, ListGroup} from "react-bootstrap"

import * as PostActions from '../../actions/PostActions'


class Comments extends React.Component{
	componentDidMount(){
		this.props.getComments(this.props.postId)
	}
	
	fillComment = () => {
		
		const comments = this.props.comments
		console.log(comments)
		// comments.map((comment)=>( 
		// 	<ListGroup.Item>comment.body</ListGroup.Item>
		// ))
	}

	render(){
		return(
			<Card>
			  <ListGroup variant="flush">
				{this.fillComment()}
			  </ListGroup>
			</Card>
		)
	}
}

const mapStateToProps = (reducers)=>{
	return reducers.PostReducer	
}

export default connect(mapStateToProps, PostActions)(Comments);