import React from 'react'
import {Table} from "react-bootstrap"
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';


class UsersTable extends React.Component {
	fillTable = () => {
		return(
			<Table striped bordered hover>
				<thead>
					<tr>
						<th>Name</th>
						<th>Email</th>
						<th>Website</th>
						<th>Actions</th>
					</tr>
				</thead>
				<tbody>
					{ this.fillColumns() }
				</tbody>
			</Table>							
		)
	}

	fillColumns = () => this.props.users.map((usuario, key) => (
		<tr key={ usuario.id }>
			<td>
				{ usuario.name }
			</td>
			<td>
				{ usuario.email }
			</td>
			<td>
				{ usuario.website }
			</td>
			<td>
				<Link className="btn btn-primary" to={`/Posts/${usuario.id}`}><i className="far fa-eye"></i></Link>
			</td>
		</tr>
	))

	render() {
		return (
			 this.fillTable() 
		)
	}
}

const mapStateToProps = (reducers)=>{
	return reducers.UsersReducer	
}

export default connect(mapStateToProps)(UsersTable);