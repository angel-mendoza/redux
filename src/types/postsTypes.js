export const GET_ALL_POST = 'get_posts'
export const LOADING = 'loading_posts'
export const ERROR = 'error_posts'
export const GET_ONE_POST = 'get_post'
export const GET_COMMENTS = 'get_comments'

