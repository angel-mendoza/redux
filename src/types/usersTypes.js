export const GET_ALL_USERS = 'get_users'
export const GET_ONE_USER = 'get_one_users'
export const LOADING = 'loading_users'
export const ERROR = 'error_users'