import React from 'react'
import {Container, Row, Col, InputGroup, FormControl, Button} from "react-bootstrap"
import { connect } from 'react-redux'
import { Redirect } from 'react-router-dom';

import Spinner from '../components/Spinner'
import ErrorMsg from '../components/Error'

import * as TaskActions from '../actions/TasksActions'

class NewTask extends React.Component{
	
	componentDidMount(){
		const {
			match: { params: {user_id , task_id}},
			tasks,
			changeTitleTask,
			changeUserId
		} = this.props 
	
		if (user_id && task_id) {
			const task = tasks[user_id][task_id]
			changeUserId(task.userId)
			changeTitleTask(task.title)
		}
	}

	changeUserId = (event) => {
		this.props.changeUserId(event.target.value)
	}

	changeTitleTask = (event) => {
		this.props.changeTitleTask(event.target.value)
	}

	saveTask = () => {
		const {
			tasks,			
			user_id, 
			title, 
			addNewTask,
			editTack
		} = this.props
		
		const new_task = {
			userId: user_id,
			title: title,
			completed: false
		}

		if (this.props.match.params.user_id && this.props.match.params.task_id) {
			const task = tasks[this.props.match.params.user_id][this.props.match.params.task_id]
			const taskEdited = {
				...new_task,
				completed: task.completed,
				id: task.id
			}
			editTack(taskEdited)
		}else{
			addNewTask(new_task)	
		}
	}

	disableBtn = () => {
		const {user_id, title, loading} = this.props

		if (loading) {
			return true
		}
		if (!user_id || !title) {
			return true
		}

		return false
	}

	showActions = () => {
		const {loading, error} = this.props
		if (loading) {
			return <Spinner />
		}
		if (error) {
			return <ErrorMsg error={error}/>
		}
	}

	render(){
		if (this.props.goBack) return <Redirect to="/tasks" />
		
		return(
			<Container>	
				<Row>
					<Col className="main">
						<h1 className="mb-5">Nueva Tarea</h1>

						<InputGroup className="mb-3">
							<InputGroup.Prepend>
								<InputGroup.Text id="basic-addon3">
									Usuario
								</InputGroup.Text>
							</InputGroup.Prepend>
							<FormControl 
								id="user" 
								type="number"
								value={this.props.user_id || ''}
								onChange={this.changeUserId} />
						</InputGroup>			

						<InputGroup className="mb-3">
							<InputGroup.Prepend>
								<InputGroup.Text id="basic-addon3">
									Tarea
								</InputGroup.Text>
							</InputGroup.Prepend>
							<FormControl 
								id="title"  
								value={this.props.title || ''} 
								onChange={this.changeTitleTask}/>
						</InputGroup>		

						<Button 
							variant="primary"
							disabled={this.disableBtn()}
							onClick={this.saveTask}
							>Guardar <i className="fas fa-save"></i>
						</Button>

						{this.showActions()}
					</Col>
				</Row>
			</Container>	
		)
	}

}

const mapStateToProps = (reducers)=>{
	return reducers.TaskReducer	
}

export default connect(mapStateToProps, TaskActions)(NewTask)