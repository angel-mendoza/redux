import React from 'react';
import { connect } from 'react-redux'
import {Container, Row, Col} from "react-bootstrap"
import * as UsersAction from '../actions/UsersAction'

import UsersTable from '../components/users/UsersTable'
import Spinner from '../components/Spinner'
import ErrorMsg from '../components/Error'

class Users extends React.Component {
	componentDidMount() {
		if (!this.props.users.length) {
			this.props.getAllUser()
		}
	}

	fillTable = () => {
		if (this.props.loading) {
			return	<Spinner />
		}

		if (this.props.error) {
			return <ErrorMsg />
		}

		return <UsersTable />		
	}

	render(){
		return(
			<Container>	
				<Row>
					<Col className="main">
						{this.fillTable()}
					</Col>
				</Row>
			</Container>		
		)
	}
}

const mapStateToProps = (reducers)=>{
	return reducers.UsersReducer	
}

export default connect(mapStateToProps, UsersAction)(Users);