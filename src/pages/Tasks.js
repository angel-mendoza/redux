import React from 'react'
import {Container, Row, Col, Button} from "react-bootstrap"
import { connect } from 'react-redux'
import { Link } from 'react-router-dom';

import * as TasksActions from '../actions/TasksActions'
import Spinner from '../components/Spinner'
import ErrorMsg from '../components/Error'

class Tasks extends React.Component{
	componentDidMount(){
		if (!Object.keys(this.props.tasks).length) {
			this.props.getAllTasks()
		}
	}

	fillPage = () => {
		const { tasks , loading , error} = this.props

		if (loading) {
			return	<Spinner />
		}

		if (error) {
			return <ErrorMsg error={error}/>
		}

		return Object.keys(tasks).map((usu_id) => (
			<div key={usu_id}>
				<h2>Usuario {usu_id}</h2>
				<div className="tasks_container">
					{this.fillTasks(usu_id)}
				</div>
			</div>
		))
	}

	fillTasks = (usu_id) =>{
		const {tasks , checkChange} = this.props
		const user_tasks = {
			...tasks[usu_id]
		}

		return Object.keys(user_tasks).map((task_id)=>(
			<div className="mb-2" key={task_id}>
				<input 
					type='checkbox' 
					onChange={ () => checkChange(usu_id, task_id)}
					defaultChecked={user_tasks[task_id].completed}
					></input>
				{
					user_tasks[task_id].title
				}
				<div className="d-ib ml-1">
						<Link className="btn btn-warning ml-2 c-w btn-sm" to={`/tasks/edit/${usu_id}/${task_id}`}>Editar <i className="fas fa-edit"></i></Link>
						<Button variant="danger" size="sm" className="ml-2">
							Eliminar <i className="fas fa-trash-alt"></i>
						</Button>
				</div>								
			</div>
		))
	}

	render(){
		
		return(
			<Container className="main">	
				<Row>
					<Col sm={12}>
						<h1 className="title-page">Tareas</h1>
						<Link className="btn btn-primary float-right" to={`/tasks/new`}>Nueva Tarea <i className="fas fa-tasks"></i></Link>
					</Col>
					<Col>
						{this.fillPage()}
					</Col>
				</Row>
			</Container>	
		)
	}
}

const mapStateToProps = (reducers)=>{
	return reducers.TaskReducer	
}

export default connect(mapStateToProps, TasksActions)(Tasks);