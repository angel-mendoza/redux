import React from 'react'
import {Container, Row, Col} from "react-bootstrap"

const NotFound = () => {
	return (
		<Container>	
			<Row>
				<Col className="pages-error">
					<h2 className="error-msg-code">404</h2>
					<h5 className="error-msg-title">Pages Not Found</h5>
				</Col>
			</Row>
		</Container>				
	)
}

export default NotFound