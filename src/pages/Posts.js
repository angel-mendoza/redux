import React from 'react'
import {Container, Row, Col, Card} from "react-bootstrap"
import { connect } from 'react-redux'
import * as UsersAction from '../actions/UsersAction'
import * as PostActions from '../actions/PostActions'

import Spinner from '../components/Spinner'
import ErrorMsg from '../components/Error'
import Comments from '../components/posts/Comments'

class Posts extends React.Component{



	async componentDidMount() {
		if (!this.props.UsersReducer.user) {
			await this.props.getOneUser(this.props.match.params.id)
		}
		this.props.getPostforUser(this.props.match.params.id)
	}

	fill = () => {
		if (this.props.UsersReducer.loading || this.props.PostReducer.loading) {
			return	<Spinner />
		}
		

		if (this.props.UsersReducer.error) {
			return <ErrorMsg error={this.props.UsersReducer.error}/>
		}

		if (this.props.PostReducer.error) {
			return <ErrorMsg error={this.props.PostReducer.error}/>
		}		

		if (!this.props.UsersReducer.user) return

		return (
			<h1 className="mb-5">Usuario {this.props.UsersReducer.user.name}</h1>

		)	
	}


	fillPosts = () => { 
		if (!this.props.PostReducer.posts) return

	
		const posts = this.props.PostReducer.posts
		
this.props.getComments()
		return(
			posts.map((post, key)=>(
				<Card className="mb-4" key={ post.id }>
				  <Card.Body>
				    <Card.Title>{post.title}</Card.Title>
				    <Card.Text>{post.body}</Card.Text>
				    <Comments postId={post.id} comments={post.comments.data} />
				  </Card.Body>
				</Card>	
			))

		)
	}

	render(){
		return(
			<Container>	
				<Row>
					<Col className="main">
						{this.fill()}
						{this.fillPosts()}
					</Col>
				</Row>
			</Container>		
		)
	}
}

const mapStateToProps = ({UsersReducer,PostReducer})=>{
	return {
		UsersReducer,
		PostReducer
	}
}

const mapDispatchProps = {
	...UsersAction,
	...PostActions
}
export default connect(mapStateToProps,mapDispatchProps)(Posts);