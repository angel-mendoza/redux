import {GET_ALL_POST, LOADING, ERROR, GET_ONE_POST, GET_COMMENTS} from '../types/postsTypes'

const INITIAL_STATE = {
	posts: [],
	loading: false,
	error: null
}

export default (state = INITIAL_STATE, action) =>{
	switch (action.type){
		case GET_ALL_POST:
			return{
				...state,
				posts: action.payload,
				loading: false,
				error: null
			}

		case GET_ONE_POST:
			return{
				...state,
				posts: action.payload,
				loading: false,
				error: null
			}
		
		case GET_COMMENTS:
			return{
				...state,
				posts: action.payload,
				loading:false,
				error:null
			}

		case LOADING: 
			return { ...state, 
				loading: true
			}
		
		case ERROR:
			return {...state, 
				error: action.payload,
				loading:false
			}
		default: return state 	
	}
}