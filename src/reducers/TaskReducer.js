import {GET_ALL_TASKS, LOADING, ERROR, CHANGE_USER_ID, CHANGE_TITLE_TASK, SAVE_TASK, TO_UPDATE} from '../types/taskTypes'

const INITIAL_STATE = {
	tasks:{},
	loading: false,
	error: null,
	user_id: null,
	title: null,
	goBack: false
}

export default (state = INITIAL_STATE, action) =>{
	switch (action.type){
		case GET_ALL_TASKS:
			return {
				...state, 
				tasks: action.payload,
				loading: false,
				error: null,
				goBack: false
			}

		case LOADING: 
			return { ...state, 
				loading: true
			}
		
		case ERROR:
			return {...state, 
				error: action.payload,
				loading:false
			}
		
		case CHANGE_USER_ID:
			return {
				...state,
				user_id: action.payload
			}
		
		case CHANGE_TITLE_TASK:
			return {
				...state,
				title: action.payload
			}

		case SAVE_TASK: 
			return {
				...state,
				tasks: {},
				loading: false,
				error: null,
				goBack:true,
				user_id: null,
				title:null
			}	
			
		case TO_UPDATE:
				return {
					...state,
					tasks: action.payload
				}	
		default: return state 	
	}
}