import { combineReducers } from 'redux'
import UsersReducer from './UsersReducer'
import PostReducer from './PostReducer'
import TaskReducer from './TaskReducer'

export default combineReducers({
	UsersReducer,
	PostReducer,
	TaskReducer
})