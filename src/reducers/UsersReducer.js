import {GET_ALL_USERS, LOADING, ERROR, GET_ONE_USER} from '../types/usersTypes'

const INITIAL_STATE = {
	users: [],
	user: null,
	loading: false,
	error: null
}

export default (state = INITIAL_STATE, action) =>{
	switch (action.type){
		case GET_ALL_USERS:
			return {
				...state, 
				users: action.payload,
				loading: false,
				error: null
			}

		case GET_ONE_USER:
			return{
				...state,
				user: action.payload,
				loading: false,
				error: null
			}

		case LOADING: 
			return { ...state, 
				loading: true
			}
		
		case ERROR:
			return {...state, 
				error: action.payload,
				loading:false
			}

		default: return state 	
	}
}