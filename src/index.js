import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import App from './components/App'
import 'bootstrap/dist/css/bootstrap.css'
import "@fortawesome/fontawesome-free/css/all.css";

import Reducers from './reducers/Reducers'

import { createStore , applyMiddleware } from 'redux'
import { Provider } from 'react-redux'

import reduxThunk from 'redux-thunk'


const store = createStore(
	Reducers,//todos los reducer
	{},//estado inicial
	applyMiddleware(reduxThunk)
)

ReactDOM.render(
	<Provider store={store}>
		<App />
	</Provider>, 
	document.getElementById('root')
)
