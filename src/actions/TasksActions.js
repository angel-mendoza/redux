import axios from 'axios'
import {GET_ALL_TASKS, LOADING, ERROR, CHANGE_USER_ID, CHANGE_TITLE_TASK, SAVE_TASK, TO_UPDATE} from '../types/taskTypes'

export const getAllTasks = () => async (dispatch) => {
	
	dispatch({
		type: LOADING
	})

	try {
		const respuesta = await axios.get('https://jsonplaceholder.typicode.com/todos');

		const tasks = {}

		respuesta.data.map((tar)=>(
			tasks[tar.userId]={
				...tasks[tar.userId],
				[tar.id]: {
					...tar
				}
			}
		))

		dispatch({
			type: GET_ALL_TASKS,
			payload: tasks
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}
};

export const changeUserId = (user_id) => (dispatch) => {
	dispatch({
		type: CHANGE_USER_ID,
		payload: user_id
	})
}

export const changeTitleTask = (title) => (dispatch) => {
	dispatch({
		type: CHANGE_TITLE_TASK,
		payload: title
	})	
}

export const addNewTask = (new_task) => async (dispatch) => {
	dispatch({
		type: LOADING
	})

	try {
		const respuesta = await axios.post('https://jsonplaceholder.typicode.com/todos', new_task);
		dispatch({
			type: SAVE_TASK
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}	
}

export const editTack = (taskEdited) => async (dispatch) =>{
	dispatch({
		type: LOADING
	})

	try {
		const respuesta = await axios.put(`https://jsonplaceholder.typicode.com/todos/${taskEdited.id}`, taskEdited);
		dispatch({
			type: SAVE_TASK
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}		
}

export const checkChange = (user_id , task_id) => (dispatch, getState) => {
	const {tasks} = getState().TaskReducer
	const selected = tasks[user_id][task_id]

	const actualizada = {
		...tasks
	}
	actualizada[user_id] = {
			...tasks[user_id]
	} 
	actualizada[user_id][task_id] = {
		...tasks[user_id][task_id],
		completed: !selected.completed
	}

	dispatch({
		type:TO_UPDATE,
		payload:actualizada
	})
}