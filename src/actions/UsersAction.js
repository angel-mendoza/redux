import axios from 'axios'
import {GET_ALL_USERS, LOADING, ERROR, GET_ONE_USER} from '../types/usersTypes'

export const getAllUser = () => async (dispatch) => {
	
	dispatch({
		type: LOADING
	})

	try {
		const respuesta = await axios.get('https://jsonplaceholder.typicode.com/users');
		dispatch({
			type: GET_ALL_USERS,
			payload: respuesta.data
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}
};


export const getOneUser = (id) => async (dispatch) => {
	
	dispatch({
		type: LOADING
	})

	try {
		const result = await axios.get(`https://jsonplaceholder.typicode.com/users/${id}`) 

		dispatch({
			type: GET_ONE_USER,
			payload: result.data
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}
};