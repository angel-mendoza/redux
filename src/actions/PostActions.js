import axios from 'axios'
import {GET_ALL_POST, LOADING, ERROR, GET_ONE_POST} from '../types/postsTypes'

export const getAllPost = () => async (dispatch) => {
	
	dispatch({
		type: LOADING
	})

	try {
		const respuesta = await axios.get('https://jsonplaceholder.typicode.com/posts');
		dispatch({
			type: GET_ALL_POST,
			payload: respuesta.data
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}
}

export const getPostforUser = (id) => async (dispatch, getState) => {
	//const {post} = getState().PostReducer 
	//https://jsonplaceholder.typicode.com/posts?userId=1
	dispatch({
		type: LOADING
	})
	
	try {
		const result = await axios.get(`https://jsonplaceholder.typicode.com/posts?userId=${id}`) 

		const nueva = result.data.map((post)=>({
			...post,
			comments: []
		}))

		dispatch({
			type: GET_ONE_POST,
			payload: nueva
		})

	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}
}

export const getComments = (id) => async(dispatch, getState) =>{
	const posts = getState().PostReducer.posts 
	dispatch({
		type: LOADING
	})

	try {
		const result = await axios.get(`https://jsonplaceholder.typicode.com/comments?postId=${id}`);
		const nueva = posts.map((posts)=>({
			...posts,
			comments:result
		}))

		dispatch({
			type: GET_ONE_POST ,
			payload: nueva
		})
	}
	catch (error) {
		dispatch({
			type: ERROR ,
			payload:error.message
		})
	}
}
